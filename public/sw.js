//INSTALACIÓN DEL SERVICE WORKER
self.addEventListener("install", function (event) {
  console.log("SW instalado");
  //self.skipWaiting();

  const instalacion = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Término de la instalación");
      self.skipWaiting();
      resolve();
    }, 1000);
  });

  event.waitUntil(instalacion);
});

//ACTIVACION DEL SW
self.addEventListener("activate", function (event) {
  console.log("SW activado");
});

//Manejo de peticiones HTTP
self.addEventListener("fetch", function (event) {
  if (event.request.url.includes("https://fakestoreapi.com/products/1")) {
    const resp = new Response(
      `{ "ok": false, "mensaje": "Recurso no disponible" } `
    );
    event.respondWith(resp);
  }

  //console.log("SW FETCH", event.request.url);
});

// SYNC
self.addEventListener("sync", function (event) {
  console.log("Tenemos conexión");
  console.log(event);
  console.log(event.log);
});
